﻿using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using SteamKit2;
using SteamKit2.Internal;

namespace SteamTools
{
    internal class Program
    {
        private static SteamClient _steamClient;
        private static CallbackManager _callbackManager;

        private static SteamUser _steamUser;
        private static SteamFriends _steamFriends;

        private static EPersonaState _personaState;

        private static bool _isRunning;
        private static string _username, _password = "", _authCode, _twoFactorAuth;

        public static void Main(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                Console.WriteLine(
                    "Welcome to SteamTools.\nYou can also use this tool from command line.\nUsage: SteamTools <username> <password>");

                Console.Write("Username: ");
                _username = Console.ReadLine();

                Console.Write("Password: ");
                // we will hide it with stars
                ConsoleKeyInfo key;
                do
                {
                    key = Console.ReadKey(true);
                    if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                    {
                        _password += key.KeyChar;
                        Console.Write("*");
                    }
                    else
                    {
                        if (key.Key != ConsoleKey.Backspace || _password.Length <= 0) continue;

                        _password = _password.Substring(0, (_password.Length - 1));
                        Console.Write("\b \b");
                    }
                } while (key.Key != ConsoleKey.Enter);
            }
            else
            {
                try
                {
                    _username = args[0];
                    _password = args[1];
                }
                catch (IndexOutOfRangeException ex)
                {
                    Console.WriteLine("Wrong arguments given.\nPress any key to exit...");
                    Console.ReadKey();
                    return;
                }
            }

            Console.WriteLine();

            // create our steamclient instance
            _steamClient = new SteamClient();

            // create the callback manager which will route callbacks to function calls
            _callbackManager = new CallbackManager(_steamClient);

            // get the steamuser handler, which is used for logging on after successfully connecting
            _steamUser = _steamClient.GetHandler<SteamUser>();
            // get the steam friends handler, which is used for interacting with friends on the network after logging on
            _steamFriends = _steamClient.GetHandler<SteamFriends>();

            // register a few callbacks we're interested in
            // these are registered upon creation to a callback manager, which will then route the callbacks
            // to the functions specified
            _callbackManager.Subscribe<SteamClient.ConnectedCallback>(OnConnected);
            _callbackManager.Subscribe<SteamClient.DisconnectedCallback>(OnDisconnected);
            _callbackManager.Subscribe<SteamUser.LoggedOnCallback>(OnLoggedOn);
            _callbackManager.Subscribe<SteamUser.LoggedOffCallback>(OnLoggedOff);
            // this callback is triggered when the steam servers wish for the client to store the sentry file
            _callbackManager.Subscribe<SteamUser.UpdateMachineAuthCallback>(OnMachineAuth);
            // we use the following callbacks for friends related activities
            _callbackManager.Subscribe<SteamUser.AccountInfoCallback>(OnAccountInfo);
            _callbackManager.Subscribe<SteamFriends.FriendMsgCallback>(OnFriendMsg);

            _callbackManager.Subscribe<SteamFriends.FriendsListCallback>(OnFriendsList);
            _callbackManager.Subscribe<SteamFriends.FriendAddedCallback>(OnFriendAdded);

            // temporary(?) connection problems
            var loadServersTask = SteamDirectory.Initialize();
            loadServersTask.Wait();
            if (loadServersTask.IsFaulted)
            {
                if (loadServersTask.Exception != null)
                    Console.WriteLine("Error loading server list from directory: {0}", loadServersTask.Exception.Message);
                return;
            }
            _isRunning = true;
            Console.WriteLine("Connecting to Steam...");
            // initiate the connection
            _steamClient.Connect();
            // create our callback handling loop
            while (_isRunning)
            {
                // in order for the callbacks to get routed, they need to be handled by the manager
                _callbackManager.RunWaitCallbacks(TimeSpan.FromSeconds(1));
            }
        }

        private static void OnFriendAdded(SteamFriends.FriendAddedCallback callback)
        {
            // someone accepted our friend request, or we accepted one
            Console.WriteLine("{0} [{1}] is now a friend", callback.PersonaName, callback.SteamID);
            // and write something to him
            _steamFriends.SendChatMessage(callback.SteamID, EChatEntryType.ChatMsg,
                string.Format("Hello {0}! Now we are friends ;)", callback.PersonaName));
        }

        // on any action with friend list (adding/removing) this callback will rign
        private static void OnFriendsList(SteamFriends.FriendsListCallback callback)
        {
            // we can also iterate over our friendslist to accept or decline any pending invites
            foreach (var friend in callback.FriendList)
            {
                if (friend.Relationship == EFriendRelationship.RequestRecipient)
                {
                    // this user has added us, let's add him back
                    _steamFriends.AddFriend(friend.SteamID);
                }
            }
        }

        private static void OnFriendMsg(SteamFriends.FriendMsgCallback callback)
        {
            // fakelog when someone actually type something to us
            if (callback.EntryType == EChatEntryType.ChatMsg)
            {
                Console.WriteLine("{3} | Message from {0}[{1}]: {2}", _steamFriends.GetFriendPersonaName(callback.Sender), callback.Sender, callback.Message, DateTime.Now.ToLocalTime().ToString(CultureInfo.InvariantCulture));
            }
            // we also can operate some commands in way we want to
            switch (callback.Message)
            {
                case "!commands":
                    _steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg, "Avaliable commands: !gold, !usual, !sleep, !awake, !info");
                    break;
                case "!gold":
                    _personaState = EPersonaState.Online;
                    var clientMsgProtobuf = new ClientMsgProtobuf<CMsgClientChangeStatus>(EMsg.ClientChangeStatus)
                    {
                        SourceJobID = _steamClient.GetNextJobID(),
                        Body =
                        {
                            persona_state = (uint) _personaState,
                            persona_state_flags = 4,
                            player_name = _steamFriends.GetPersonaName()
                        }
                    };
                    clientMsgProtobuf.SourceJobID = _steamClient.GetNextJobID();

                    _steamClient.Send(clientMsgProtobuf);
                    break;
                case "!usual":
                    _personaState = EPersonaState.Online;
                    _steamFriends.SetPersonaState(_personaState);
                    break;
                case "!sleep":
                    _personaState = EPersonaState.Snooze;
                    _steamFriends.SetPersonaState(_personaState);
                    break;
                case "!awake":
                    _personaState = EPersonaState.Online;
                    _steamFriends.SetPersonaState(_personaState);
                    break;
                case "!disconnect":
                    _steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg,
                        "Bye bye (:\nBot version: 1");
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                    _steamUser.LogOff();
                    _steamClient.Disconnect();
                    _isRunning = false;
                    break;
                case "!info":
                    _steamFriends.SendChatMessage(callback.Sender, EChatEntryType.ChatMsg,
                        string.Format("Your steam_id is: {0}", callback.Sender.Render()));
                    break;
            }
        }

        private static void OnConnected(SteamClient.ConnectedCallback callback)
        {
            if (callback.Result != EResult.OK)
            {
                Console.WriteLine("Unable to connect to Steam: {0}", callback.Result);

                _isRunning = false;

                return;
            }

            Console.WriteLine("Connected to Steam! Logging in '{0}'...", _username);

            byte[] sentryHash = null;
            if (File.Exists("sentry.bin"))
            {
                // if we have a saved sentry file, read and sha-1 hash it
                // ReSharper disable once SuggestVarOrType_Elsewhere
                byte[] sentryFile = File.ReadAllBytes("sentry.bin");
                sentryHash = CryptoHelper.SHAHash(sentryFile);
            }

            _steamUser.LogOn(new SteamUser.LogOnDetails
            {
                Username = _username,
                Password = _password,

                LoginID = 0,

                // we pass in an additional authcode
                // this value will be null (which is the default) for our first logon attempt
                AuthCode = _authCode,
                // if the account is using 2-factor auth, we'll provide the two factor code instead
                // this will also be null on our first logon attempt
                TwoFactorCode = _twoFactorAuth,
                // our subsequent logons use the hash of the sentry file as proof of ownership of the file
                // this will also be null for our first (no authcode) and second (authcode only) logon attempts
                SentryFileHash = sentryHash
            });
        }

        private static void OnDisconnected(SteamClient.DisconnectedCallback callback)
        {
            // after recieving an AccountLogonDenied, we'll be disconnected from steam
            // so after we read an authcode from the user, we need to reconnect to begin the logon flow again
            Console.WriteLine("Disconnected from Steam, reconnecting in 5...");

            Thread.Sleep(TimeSpan.FromSeconds(5));

            _steamClient.Connect();
        }

        private static void OnLoggedOn(SteamUser.LoggedOnCallback callback)
        {
            var isSteamGuard = callback.Result == EResult.AccountLogonDenied;
            var is2FactorAuth = callback.Result == EResult.AccountLoginDeniedNeedTwoFactor;

            if (isSteamGuard || is2FactorAuth)
            {
                Console.WriteLine("This account is SteamGuard protected!");

                if (is2FactorAuth)
                {
                    Console.Write("Please enter your 2 factor auth code from your authenticator app: ");
                    _twoFactorAuth = Console.ReadLine();
                }
                else
                {
                    Console.Write("Please enter the auth code sent to the email at {0}: ", callback.EmailDomain);
                    _authCode = Console.ReadLine();
                }

                return;
            }

            if (callback.Result != EResult.OK)
            {
                Console.WriteLine("Unable to logon to Steam: {0} / {1}", callback.Result, callback.ExtendedResult);

                _isRunning = false;

                return;
            }

            Console.WriteLine("Successfully logged on!");
        }

        public static void OnAccountInfo(SteamUser.AccountInfoCallback callback)
        {
            // before being able to interact with friends, you must wait for the account info callback
            // this callback is posted shortly after a successful logon
            // at this point, we can go online on friends, so lets do that
            _personaState = EPersonaState.Online;

            _steamFriends.SetPersonaState(_personaState);
        }

        private static void OnLoggedOff(SteamUser.LoggedOffCallback callback)
        {
            Console.WriteLine("Logged off of Steam: {0}", callback.Result);
        }

        private static void OnMachineAuth(SteamUser.UpdateMachineAuthCallback callback)
        {
            Console.WriteLine("Updating sentryfile...");
            // write out our sentry file
            // ideally we'd want to write to the filename specified in the callback
            // but then this sample would require more code to find the correct sentry file to read during logon
            // for the sake of simplicity, we'll just use "sentry.bin"
            int fileSize;
            byte[] sentryHash;
            using (var fs = File.Open("sentry.bin", FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                fs.Seek(callback.Offset, SeekOrigin.Begin);
                fs.Write(callback.Data, 0, callback.BytesToWrite);
                fileSize = (int)fs.Length;
                fs.Seek(0, SeekOrigin.Begin);
                using (var sha = new SHA1CryptoServiceProvider())
                {
                    sentryHash = sha.ComputeHash(fs);
                }
            }

            // inform the steam servers that we're accepting this sentry file
            _steamUser.SendMachineAuthResponse(new SteamUser.MachineAuthDetails
            {
                JobID = callback.JobID,

                FileName = callback.FileName,

                BytesWritten = callback.BytesToWrite,
                FileSize = fileSize,
                Offset = callback.Offset,

                Result = EResult.OK,
                LastError = 0,

                OneTimePassword = callback.OneTimePassword,

                SentryFileHash = sentryHash
            });
            Console.WriteLine("Done!");
        }
    }
}
